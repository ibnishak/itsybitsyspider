#+TITLE:  Emacs: Notes on Keybindings in Linux
#+SUBTITLE:  
#+AUTHOR:    Rizwan Ishak
#+EMAIL:     ibnishak@live.com
#+DESCRIPTION: A saner way to use keybindings in emacs, especially for those who are new to emacs
#+KEYWORDS:  Emacs, Org mode, Keybindings, general.
#+LANGUAGE:  en

* Pointless introduction
Emacs being a rather keyboard driven editor, tends to rely heavily on keyboard shortcuts. The non-conformity of keybindings in emacs is a rather tricky hurdle. While it is accepted that Emacs is practically an operating system and the true emacs-jedi can do everything in Emacs itself, the rest of mortals have to interact with other, normal softwares. Let us consider the simple tasks of cut, copy and paste. Everyone and their grandmothers know that the shortcut for these are @@html:<kbd>@@Ctrl+x@@html:</kbd>@@, @@html:<kbd>@@Ctrl+c@@html:</kbd>@@ and @@html:<kbd>@@Ctrl+v@@html:</kbd>@@. If you happen to flirt with terminals, then you need to remember that in most terminals the shortcuts are @@html:<kbd>@@Ctrl+Shift+c@@html:</kbd>@@ and @@html:<kbd>@@Ctrl+Shift+v@@html:</kbd>@@. Now come to emacs, where the keybindings become @@html:<kbd>@@Ctrl+w@@html:</kbd>@@, @@html:<kbd>@@Alt+w@@html:</kbd>@@ and @@html:<kbd>@@Ctrl+y@@html:</kbd>@@. The toll it takes on your  muscle memory and frustration that ensues everytime you send SIGKILL instead of copying is no funny buisness.

* Plan
We are going to set up Emacs such that we can use the all the standard keybindings like: 
@@html:<kbd>@@Ctrl+a@@html:</kbd>@@ to Select all
@@html:<kbd>@@Ctrl+s@@html:</kbd>@@ to Save
@@html:<kbd>@@Ctrl+d@@html:</kbd>@@ to add bookmark
@@html:<kbd>@@Ctrl+f@@html:</kbd>@@ to find
@@html:<kbd>@@Ctrl+z@@html:</kbd>@@ to undo
@@html:<kbd>@@Ctrl+x@@html:</kbd>@@ to cut
@@html:<kbd>@@Ctrl+c@@html:</kbd>@@ to copy
@@html:<kbd>@@Ctrl+v@@html:</kbd>@@ to paste

Since Emacs heavily uses Ctrl and Alt keys, we need to avoid messing with these modifier keys. So here is the plan
- Use =xmodmap= to convert @@html:<kbd>@@CapsLock@@html:</kbd>@@ to @@html:<kbd>@@Hyper@@html:</kbd>@@ key. So for every other software except Emacs, @@html:<kbd>@@CapsLock@@html:</kbd>@@ acts as @@html:<kbd>@@Hyper@@html:</kbd>@@. All other modifier keys remain at their default position and usage.


- In Emacs and Emacs alone, swap the @@html:<kbd>@@Hyper@@html:</kbd>@@ and @@html:<kbd>@@Ctrl@@html:</kbd>@@ keys, so that when you press @@html:<kbd>@@Ctrl@@html:</kbd>@@ button in keyboard, @@html:<kbd>@@Hyper@@html:</kbd>@@ is sent to Emacs, while pressing @@html:<kbd>@@CapsLock@@html:</kbd>@@ button sends @@html:<kbd>@@Ctrl@@html:</kbd>@@ to Emacs.


- Finally set up keybindings like @@html:<kbd>@@Hyper-x@@html:</kbd>@@  for cut, @@html:<kbd>@@Hyper-c@@html:</kbd>@@  for copy in Emacs. Since Emacs is accepting @@html:<kbd>@@Ctrl@@html:</kbd>@@ as @@html:<kbd>@@Hyper@@html:</kbd>@@, the actual keypress would be @@html:<kbd>@@Ctrl+x@@html:</kbd>@@, @@html:<kbd>@@Ctrl+y@@html:</kbd>@@ etc
* Step 1: Xmodmap
- Run the following command

#+BEGIN_SRC :tangle no 
  xmodmap -pke > ~/.Xmodmap
#+END_SRC

- Open ~/.Xmodmap. Add the following code to the file.


#+BEGIN_SRC :tangle no 
    clear      lock 
    clear      mod1
    clear      mod2
    clear      mod3
    clear      mod4
    clear      mod5
    keycode      66 = Hyper_L
    add        mod1 = Alt_L Alt_R Meta_L
    add        mod2 = Num_Lock
    add        mod3 = Hyper_L
    add        mod4 = Super_L Super_R
    add        mod5 = Mode_switch ISO_Level3_Shift
#+END_SRC


- Reload the xmodmap by running
#+BEGIN_SRC :tangle no 
  xmodmap ~/.Xmodmap
#+END_SRC
#+ATTR_HTML: :class alert-warning
#+BEGIN_alert
It is possible that keycodes vary in your keyboard. If the above codes do not work after reloading, try using @@html:<a href="https://linux.die.net/man/1/xev" class="alert-link">@@xev@@html:</a>@@ 
#+END_alert

* Step 2: Emacs
Add the following snippet to your =init.el= in *Linux OS*. If you are using Windows/Mac, find appropriate snippet from [[http://ergoemacs.org/emacs/emacs_hyper_super_keys.html][ergoemacs]].
#+ATTR_HTML: :copy-button t
#+BEGIN_SRC emacs-lisp :tangle no 
    (setq x-ctrl-keysym 'hyper) ;;In Emacs, treat Control key as hyper
    (setq x-hyper-keysym 'ctrl) ;; In Emacs, treat Hyper Key(Caps_Lock)  as Control
#+END_SRC
* My Keybindings
I am using [[https://github.com/noctuid/general.el][general.el]] for my keybindings. A subset of which looks as follows.

#+ATTR_HTML: :class alert-primary
#+BEGIN_alert
The following snippet depends on packages viz; general.el, helm, undo-tree, helm-swoop, avy, anzu, yasnippet. You may modify the packages as per your personal preferences 
#+END_alert

#+ATTR_HTML: :copy-button t
#+BEGIN_SRC emacs-lisp :tangle no 
    (use-package general
      :defer 2
      :init
      (general-define-key
       :states '(normal insert visual)
       :keymaps 'override
       "M-x" 'helm-M-x
       "H-a" 'mark-whole-buffer
       "H-s" 'save-buffer
       "H-d" 'bookmark-set

       "H-z" 'undo-tree-undo
       "H-Z" 'undo-tree-redo ;; Ctrl+Shift+z for Redo.
       "H-x" 'kill-region
       "H-c" 'copy-region-as-kill
       "H-v" 'yank

       "H-f" 'helm-swoop
       "H-F" 'helm-projectile-ag
       "H-o" 'fzf-projectile     
       "H-n" 'helm-find-files ;; Ctrl+n for new files
       "H-w" 'kill-this-buffer  ;; Ctrl+w kills buffer
       "H-g" 'avy-goto-char-2
       "H-h" 'anzu-query-replace
       "H-H" 'anzu-query-replace-regexp   

       "H-<tab>" 'helm-mini
       "H-*" 'bookmark-jump
       "H-y"  'yas-insert-snippet
       "H-`" 'helm-register
       "H-M-`" 'point-to-register
       ))


#+END_SRC
* 13^th Function Key
Another key in the keyboard I hardly ever find using is the =Menu= key. You know, [[https://www.howtogeek.com/wp-content/uploads/2015/07/a-keyboard-shortcut-that-can-be-used-in-place-of-the-context-menu-key-01.jpg][that key]] smugly sitting in between @@html:<kbd>@@Right AltGr@@html:</kbd>@@ and @@html:<kbd>@@Right Ctrl@@html:</kbd>@@. I configured the Xmodmap so that it will act as @@html:<kbd>@@F13@@html:</kbd>@@. Since no normal keyboard has an @@html:<kbd>@@F13@@html:</kbd>@@ key, no shortcuts are assigned to this in any software. This gives us a array  of possibilities regarding keybindings devoid of headaches. Add the following line to your =~/.Xmodmap=
#+BEGIN_SRC sh :tangle no 
  keycode 135 = F13
#+END_SRC
and reload your xmodmap by running
#+BEGIN_SRC :tangle no 
  xmodmap ~/.Xmodmap
#+END_SRC

* Right Alt as F13
If =Menu= key is hard to reach for you, the Right Alt key is another option to be morphed into F13. The =~/.Xmodmap= will look like this:
#+BEGIN_SRC sh :tangle no 
    clear      lock 
    clear      mod1
    clear      mod2
    clear      mod3
    clear      mod4
    clear      mod5
    keycode      66 = Hyper_L
    keycode     108 = F13
    add        mod1 = Alt_L Meta_L
    add        mod2 = Num_Lock
    add        mod3 = Hyper_L
    add        mod4 = Super_L Super_R
    add        mod5 = Mode_switch ISO_Level3_Shift
#+END_SRC

and reload your xmodmap by running
#+BEGIN_SRC :tangle no 
  xmodmap ~/.Xmodmap
#+END_SRC
* Unrelated tip
Since you are in the business of modifying =~/.Xmodmap=, you may want to know that it can be also used to change the scrolling using mouse to [[https://jessequinnlee.com/2015/07/25/natural-scrolling-vs-reverse-scrolling/][natural scrolling]] like those seen in Mac, as opposed to the default reverse scrolling. Here is the line to add to =~/.Xmodmap=. 
#+BEGIN_SRC sh :tangle no 
  pointer = 1 2 3 5 4 7 6 8 9 10 11 12
#+END_SRC
and reload your xmodmap by running
#+BEGIN_SRC :tangle no 
  xmodmap ~/.Xmodmap
#+END_SRC
