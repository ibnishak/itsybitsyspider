#+TITLE: Mental Models:  Size instinct
#+CREATED: 2019-05-16T21:34:24+0530
#+DESCRIPTION:
#+KEYWORDS:
#+TAGS:
#+SOURCE:



Recognize when a lonely number seems impressive (small or large), and remember that you could get the opposite impression if it were compared with or divided by some other relevant number.
To control the size instinct, get things in proportion.

- Compare. Big numbers always look big. Single numbers on their own are misleading and should make you suspicious. Always look for comparisons. Ideally, divide by something.
- 80/20. Have you been given a long list? Look for the few largest items and deal with those first. They are quite likely more important than all the others put together.
- Divide. Amounts and rates can tell very different stories. Rates are more meaningful, especially when comparing between different-sized groups. In particular, look for rates per person when comparing between countries or regions.
