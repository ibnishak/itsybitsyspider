#+TITLE: ItsyBitsySpider
#+SUBTITLE: Verba volant, scripta manent




This is my blog. There are many blogs in the world, but this is mine.
This is a running log of my little experiments, and a notebook of what I consume from my surroundings.
This is also an archive of my ideas, and my digital ventriloquist dummy.
As I said, it is my blog. No blog is more dearer to me than my blog. 
 
Verba volant, scripta manent
